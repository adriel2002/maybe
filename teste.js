var product = "Socks";
//var description = "A pair of warm, fuzzy socks";

var app = new Vue({
   el: '#app',
   data: {
      brand: 'Vue Mastery',
      product: "Socks",
      selectedVariant: 0,
      link: "https://www.focusconcursos.com.br",
      details: ["80% cotton", "20% polyester", "Gender-Neutral"],

      variants: [
         {
            variantId: 2234,
            variantColor: "Green",
            variantImage: "https://www.vuemastery.com/images/challenges/vmSocks-green-onWhite.jpg",
            variantQuantity: 10
         },
         {
            variantId: 2235,
            variantColor: "Blue",
            variantImage: "https://www.vuemastery.com/images/challenges/vmSocks-blue-onWhite.jpg",
            variantQuantity: 0
         }
      ],

      sizes: [
         {
            sizeId: 2291,
            sizefeet: "40"
         },
         {
            sizeId: 2292,
            sizefeet: "39"
         }
      ],
      cart: 0,
      onSale: true
   },
   methods: {
      addToCart: function () {
         this.cart += 1
      },

      updateProduct: function (index) {
         this.selectedVariant = index
         console.log(index)
      },
      lessToCart: function () {
            if (cart >= 1) {
            this.cart -=1
      }     
   }
}, 
   computed: {
      title() {
         return this.brand + ' ' + this.product
      },
      image() {
         return this.variants[this.selectedVariant].variantImage
      },
      inStock()  {
         return this.variants[this.selectedVariant].variantQuantity
      },
         sale() {
            if (this.onSale) {
              return this.brand + ' ' + this.product + ' are on sale!'
            } 
              return  this.brand + ' ' + this.product + ' are not on sale'
          }
      }
    })

/*var app2 = new Vue({
   el: '#app-2',
   data: {
      description: "A pair of warm, fuzzy socks"
   }
})
*/