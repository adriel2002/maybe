for (var i=0; i<=7; i++) {
    var pyramide = '';

    for (var j=1; j <= i; j++) {
        pyramide +='#';
    }

    console.log(pyramide);

}

let number = 0;
number2 = number/3;
do {
    number +=1
    if ((number%3 == 0) && (number%5 == 0)) {
        console.log("FizzBuzz");
    } else if (number%5 == 0) {
        console.log("Buzz");
    } else if (number%3 == 0) {
        console.log("Fizz");
    } else {
        console.log(number);
    }
} while (number <= 99);

// VUE

<div id="app">
    {{ message }}
  </div>

  <script> 
    var app = new Vue({
      el: '#app',
      data: {
        message: 'Hello Vue!'
      }
    })

 </script><div id="app-2">
        <span v-bind:title="message">
          Hover your mouse over me for a few seconds
          to see my dynamically bound title!
        </span>
      </div>

      <div id="app-3">
        <span v-if="seen">Now you see me</span>
      </div>

      <div id="app-4">
        <ol>
          <li v-for="todo in todos">
            {{ todo.text }}
          </li>
        </ol>
      </div>

      <div id="app-5">
        <p>{{ message }}</p>
        <button v-on:click="reverseMessage">Reverse Message</button>
      </div>

      <div id="app-6">
        <p>{{ message }}</p>
        <input v-model="message">
      </div>

    <script>

      var app2 = new Vue({
        el: '#app-2',
        data: {
          message: 'You loaded this page on ' + new Date().toLocaleString()
        }
      })

      var app3 = new Vue({
  el: '#app-3',
  data: {
    seen: true
  }
})

var app4 = new Vue({
  el: '#app-4',
  data: {
    todos: [
      { text: 'Learn JavaScript' },
      { text: 'Learn Vue' },
      { text: 'Build something awesome' }
    ]
  }
})

var app5 = new Vue({
  el: '#app-5',
  data: {
    message: 'Paralelepípedo'
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  }
})

var app6 = new Vue({
  el: '#app-6',
  data: {
    message: 'Hello Vue!'
  }
})</script>

/*let a = Number(prompt("Which size?"));
 let b = 0;
 console.log("Let's play chess");
 do {
        
      console.log("# # # #");
     if (a*0.5 == 0) {
     console.log(" # # # #");
     b+=2;
     } else {
        console.log(" # # # #")
        b+=1;
   }
 } while (b != a);
*/


<!DOCTYPE html>
<html>
<head>
  <title>My first Vue app</title>
  <meta charset="UTF-8">
  <script src="https://unpkg.com/vue"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

  <div class="nav-bar"> </div>

      <div id="app">
        <div class="product">

          <div class="product-image">
            <img v-bind:src="image">
          </div>
      
          <div class="product-info">
        <h1> {{ title }} </h1>
        <p v-if="InStock"> In Stock</p>
        <p v-else :class="{ outOfStock: !InStock }"> Out of stock</p>
        <p>{{ sale }}</p>>
      
            <ul>
              <li v-for="detail in details">{{ detail }}</li>
            </ul>

            <div v-for="(variant, index) in variants" 
            :key="variant.variantId"
            class="color-box"
            :style="{ backgroundColor: variant.variantColor } "
            @mouseover="updateProduct(variant.variantImage)">
            </div>
            

            <div v-for="size in sizes" :key="size.sizeId">
              <p>{{ size.sizefeet }}</p>
            </div>
              </div>

              <button v-on:click="addToCart" :disable="!InStock"
              :class="{ disabledButton: !InStock}">Add to Cart</button>

              <div class="cart">
                <p> Cart ({{cart}})</p>
              </div>

              <button v-on:click="lessToCart">Remove from Cart</button>
      </div>

  </div>
</div>

  <script src="teste.js"></script>
</body>
</html>